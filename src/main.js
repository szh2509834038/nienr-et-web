import Vue from 'vue'
import App from './App.vue'
import store from '@/store/index.js';
import { commonAjax,commonAjaxGet } from '@/vendors/request/index.js'

import uView from "uview-ui";
Vue.use(uView);

// #ifdef H5
import eruda from "eruda";
eruda.init();
// #endif

Vue.prototype.$commonAjax = commonAjax
Vue.prototype.$commonAjaxGet = commonAjaxGet
Vue.prototype.$store = store

App.mpType = 'app'
const app = new Vue({
  store,
  ...App
})
app.$mount()

