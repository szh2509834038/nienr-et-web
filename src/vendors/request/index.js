import Request from './luch-request.js'
import {toast} from '@/api/utils.js';

const http = new Request()
const errorInfo = "服务器内部错误!";

function getBaseUrl() {
	if (process.env.NODE_ENV == 'development') {
		return '/proxy-api'
	} else {
		return "/NIS"
	}
}

export function commonAjaxGet(path, params, config = {hideLoading:false}) {
	return http.request({
		method: "GET", // 请求方法必须大写
		url: path,
		data: params,
		custom: config,
		header: {
			'Content-Type': 'application/json;charset=UTF-8'
		}
	})
}

export function commonAjax(path, params, config = {hideLoading:false}) {
	return http.request({
		method: "POST", // 请求方法必须大写
		url: path,
		data: params,
		custom: config,
		header: {
			'Content-Type': 'application/json;charset=UTF-8'
		}
	})
}

http.setConfig((config) => {
	/* 设置全局配置 */
	config.baseURL = getBaseUrl() /* 根域名不同 */
	config.header = {
		...config.header,
	}
	return config
})


http.interceptors.request.use(
	(config) => {
		//console.log("request:", JSON.stringify(config));
		if (config.custom && !config.custom.hideLoading) {
			uni.showLoading({
				mask: true,
				title: "请稍候"
			});
		}
		return config
	},
	(error) => {
		toast("错误的请求!");
		return Promise.reject(error);
	}
)


http.interceptors.response.use(
	async (response) => {
		/* 请求之后拦截器 */
		const config = response.config || {};
		uni.hideLoading();
		//console.log("response:", JSON.stringify(response));
		var code = response && response.data && response.data.ReType;
		switch (code) {
			case 0:
				return {err: null, res: response.data};
			default:
				const info = response && response.data && response.data.Msg || errorInfo;
				config.custom && !config.custom.hideError && toast(info);
				return Promise.resolve({err: new Error(info), res: response && response.data || null});
		}
	},
	(error) => {
		uni.hideLoading();
		console.log("response;error:",error);
		const config = error.config || {};
		error.errMsg && (error.errMsg == "request:ok") && (error.errMsg = "服务器内部错误");
		const info = error && error.data && error.data.msg || error.errMsg || errorInfo;
		config.custom && !config.custom.hideError && toast(info);
		return Promise.resolve({err: new Error(info), res: error && error.data || null})
	}
)


