import Vue from "vue"
import Vuex from "vuex"
Vue.use(Vuex)
const store = new Vuex.Store({
	state: {
		loginUser:null,
		area:[],

		skinTestPatientList:[//皮试病人列表
			// {
			//	brxm:'',//病人姓名
			// 	mzhm:'',// 病案号
			// 	idVismed:'',// 就诊流程号
			// 	idMedord:'',// 医嘱组号
			//  naSrvMed:'',//药品名称
			// 	quanSkintestLimit:'',// 皮试时间  分钟
			// 	startTime:'',// 开始时间  时间戳
			// 	shouldEndTime:'',// 应当结束的时间  时间戳
			// }
		],
	},
	getters: {
		getUserInfo(state){
			return state.loginUser;
		}
	},
	mutations: {
		setLoginUser(state, data){
			state.loginUser=data;
		},
		setArea(state, data){
			state.area=data;
		},

		//新增皮试病人
		addSkinTestPatient(state,patient){
			state.skinTestPatientList.push(patient);
		},
		//删除皮试病人
		deleteSkinTestPatient(state,patient){
			let patientIndex = state.skinTestPatientList.findIndex(el=>el.idVismed == patient.idVismed);
			if(patientIndex != -1){
				state.skinTestPatientList.splice(
					patientIndex,
					1
				)
			}
		}
	}
})

export default store
