function getDateTimeString(time) {
    if (time) {
        let val = new Date(time);
        let Y = val.getFullYear();
        let M = (val.getMonth() + 1) < 10 ? '0' + (val.getMonth() + 1) : (val.getMonth() + 1);
        let D = val.getDate() < 10 ? '0' + val.getDate() : val.getDate();
        let h = val.getHours() < 10 ? '0' + val.getHours() : val.getHours();
        let m = val.getMinutes() < 10 ? '0' + val.getMinutes() : val.getMinutes();
        let s = val.getSeconds() < 10 ? '0' + val.getSeconds() : val.getSeconds();
        return `${Y}-${M}-${D} ${h}:${m}:${s}`;
    }
    return ''
}
function getDateString(time) {
    if (time) {
        let val = new Date(time);
        let Y = val.getFullYear();
        let M = (val.getMonth() + 1) < 10 ? '0' + (val.getMonth() + 1) : (val.getMonth() + 1);
        let D = val.getDate() < 10 ? '0' + val.getDate() : val.getDate();
        return `${Y}-${M}-${D}`;
    }
    return ''
}
/**
 * 获取医嘱类型
 */
function getAdviceType(type) {
    if (!type) return '';
    let str = type.toString();
    if (/^1\d+$/.test(str)) {
        return '药'; //药品
    }
    if (/^21\d+$/.test(str)) {
        return '查'; //检查
    }
    if (/^22\d+$/.test(str)) {
        return '验'; //检验
    }
    if (/^23\d+$/.test(str)) {
        return '术'; //手术
    }
    if (/^24\d+$/.test(str)) {
        return '疗'; //治疗
    }
    if (/^25\d+$/.test(str)) {
        return '护'; //护理
    }
    if (/^26\d+$/.test(str)) {
        return '托'; //嘱托
    }
    if (/^27\d+$/.test(str)) {
        return '诊'; //诊疗
    }
    return '';
}

/**
 * 获取检验类型
 * @param {*} type 
 */
function getInspectType(type) {
    if (!type) return '';
    let str = type.toString();
    if (/^1\d+$/.test(str)) {
        return '药'; //药品
    }
    if (/^21\d+$/.test(str)) {
        return '查'; //检查
    }
    if (/^22\d+$/.test(str)) {
        return '验'; //检验
    }
    if (/^23\d+$/.test(str)) {
        return '术'; //手术
    }
    if (/^24\d+$/.test(str)) {
        return '疗'; //治疗
    }
    if (/^25\d+$/.test(str)) {
        return '护'; //护理
    }
    if (/^26\d+$/.test(str)) {
        return '托'; //嘱托
    }
    if (/^27\d+$/.test(str)) {
        return '诊'; //诊疗
    }
    return '';
}

/**
 * 获取医嘱计划状态
 * @param {} code 
 * 医嘱状态 0：未核对 1：已核对  2：已配药 3:已执行
 */
function getPlanStatus(code) {
    if (code == 3) {
        return '已执行';
    } else {
        if (code == 2) {
            return '未执行';
        } else {
            if (code == 1) {
                return '未配药';
            } else {
                return '未核对'
            }
        }
    }
}

//获取血袋状态
function getBloodBagStatus(status) {
    switch (status) {
        case '-1':
            return '已作废';
        case '0':
            return '已出库';
        case '1':
            return '已签收';
        case '2':
            return '已取血';
        case '3':
            return '输血开始';
        case '4':
            return '输血开始后15分钟';
        case '5':
            return '输血结束';
        case '6':
            return '输血结束后一小时';
        case '7':
            return '已上交';
        case '8':
            return '已回收';
        default:
            return '未知'
    }
}

/**
 * 获取时间戳
 */
function getTimestamp(date) {
    let date_ = new Date(date);
    return date_.valueOf();
}

/**
 * uniapp消息提示
 */
function showToast(type,msg,duration = 2500){
    if(type == 0){//操作成功
        uni.showToast({
            title: msg,
            image: "/static/check/success.svg",
            duration: duration,
        })
    }else{//操作失败
        uni.showToast({
            title: msg,
            image: "/static/check/warning.svg",
            duration: duration,
        })
    }
}

export {
    getDateTimeString,
    getAdviceType,
    getPlanStatus,
    getTimestamp,
    getInspectType,
    getDateString,
    getBloodBagStatus,
    showToast,
}