const path = require('path');

function resolve(dir) {
	return path.join(__dirname, dir);
}
module.exports = {
	transpileDependencies: ['uview-ui','@dcloudio/uni-ui'],
	chainWebpack: config => {
	},
	publicPath: './',
	devServer: {
		client: {
			overlay: false,
		},
		open: false,
		host: '0.0.0.0',
		port: 8086,
		proxy: {
			"/proxy-api": {
			  target: 'http://10.10.2.147:9066/NIS/',//测试环境
			//   target: 'http://10.0.46.42:8082/NIS/',//李洪哲 后端
			//   target: 'http://10.188.199.98:8082/NIS/',// 瑞安现场 李洪哲 后端
			//   target: 'http://172.16.220.14:8040/NIS/',//瑞安现场测试环境
			  changeOrigin: true,
			  onProxyRes(proxyRes) {
				const key = "set-cookie"
				if (proxyRes.headers[key]) {
				  const cookies = proxyRes.headers[key].join("").split(" ")
				  proxyRes.headers[key] = [cookies[0], "Path=/"].join(" ")
				}
			  },
			  pathRewrite: {
				"/proxy-api": "",
			  },
			}
		}
	}
};
